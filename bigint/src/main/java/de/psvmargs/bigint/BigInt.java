package de.psvmargs.bigint;

import java.util.Arrays;
/**
 * Class for doing big integer math, like BigInteger.
 *
 * <pre>
 * TODOs:
 *
 * implement:
 * - toString(int radix)
 * - some other BigInteger methods
 *
 * improve:
 * - JUnit-Tests
 * - JavaDoc comments
 *
 * optimize:
 * - div
 * - mul: implement karatsuba for "large" numbers
 * - toString
 *
 * </pre>
 *
 * @author agu
 */
public final class BigInt implements Comparable<BigInt> {

  public static final BigInt ZERO = new BigInt(new int[] {0}, 0);
  public static final BigInt ONE = new BigInt(new int[] {1}, 1);
  public static final BigInt TWO = new BigInt(new int[] {2}, 1);
  public static final BigInt TEN = new BigInt(new int[] {10}, 1);

  private static final long TWO_POW_32 = 1L << 32;
  private static final long LOW_32BIT_LONG_MASK = 0xFFFF_FFFF;

  private final int[] bits;
  private final int sign;

  /**
   * Returns a BigInt from an int
   *
   * @param value the number
   * @return a {@link BigInt} instance
   */
  public static BigInt of(int value) {
    if (value == 0) return ZERO;
    if (value == 1) return ONE;
    if (value == 2) return TWO;
    if (value == 10) return TEN;
    final int sign;
    long a = value;
    if (a < 0) {
      sign = -1;
      a = -a;
    } else if (a > 0) sign = 1;
    else sign = 0;

    final int[] bits = new int[] {(int) a};
    return new BigInt(bits, sign);
  }

  /**
   * Returns a BigInt from an int[] and a sign value
   *
   * @param bits representing the absolute value
   * @param sign -1 for a negative number, 0 for zero, 1 for positive number
   * @return a {@link BigInt} instance
   */
  public static BigInt of(int[] bits, int sign) {
    if (bits == null) throw new NullPointerException();
    if (bits.length == 0) throw new IllegalArgumentException("Empty bits array not allowed.");
    if ((sign == 0) && ((bits.length != 1) || (bits[0] != 0)))
      throw new IllegalArgumentException("If sign == 0, bits length must be 1, a single 0");
    if ((sign != 0) && (sign != 1) && (sign != -1))
      throw new IllegalArgumentException("Illegal sign value: " + sign);

    if (sign == 0) return ZERO;
    if ((bits.length == 1) && (sign == 1)) {
      if (bits[0] == 1) return ONE;
      if (bits[0] == 2) return TWO;
      if (bits[0] == 10) return TEN;
    }
    return new BigInt(bits.clone(), sign);
  }

  /**
   * Returns a {@link BigInt} instance whose value is equal to {@code value}
   *
   * <p>'-' and '+' as first character are allowed
   *
   * @param value the value as a binary string
   * @return a {@link BigInt} instance with the specified value
   */
  public static BigInt ofBinary(String value) {
    if (value == null) throw new NullPointerException();
    if (value.length() == 0) throw new IllegalArgumentException("Empty string not allowed.");

    int sign = 1;
    int start = 0;
    if (value.charAt(0) == '-') {
      sign = -1;
      start = 1;
    } else if (value.charAt(0) == '+') start = 1;

    BigInt result = ZERO;
    for (int i = start; i < value.length(); i++) {
      result = result.shiftLeft();
      char ch = value.charAt(i);
      if (ch == '1') result = result.add(ONE);
      else if (ch != '0') throw new IllegalArgumentException("Invalid character: " + ch);
    }
    if ((sign == -1) && !result.equals(ZERO)) return new BigInt(result.bits, -1);
    return result;
  }

  private BigInt(int[] bits, int sign) {
    this.bits = bits;
    this.sign = sign;
  }

  /**
   * Returns a BigInt whose value is {@code (-this)}.
   *
   * @return {@code (-this)}.
   */
  public BigInt negate() {
    return new BigInt(bits, -sign);
  }

  /**
   * Returns a BigInt whose value is {@code (this + addend)}.
   *
   * @param addend value which is added to this BigInt.
   * @return {@code this + addend}
   */
  public BigInt add(BigInt addend) {
    if (isZero()) return addend;
    if (addend.isZero()) return this;
    if (sign == addend.sign) {
      int[] addedBits = addBits(this.bits, addend.bits);
      return new BigInt(addedBits, sign);
    }
    if (sign == -1) return addend.subtract(this);
    return subtract(addend);
  }

  private int[] addBits(final int[] bitsA, final int[] bitsB) {
    int[] smallerLengthBits = bitsA, largerLengthBits = bitsB;
    if (smallerLengthBits.length > largerLengthBits.length) {
      smallerLengthBits = bitsB;
      largerLengthBits = bitsA;
    }
    int[] sumBits = new int[largerLengthBits.length];
    // adding bits in range [0..bitsA.length[ from both arrays
    long carry = 0;
    for (int i = 0; i < smallerLengthBits.length; i++) {
      long a = Integer.toUnsignedLong(smallerLengthBits[i]);
      long b = Integer.toUnsignedLong(largerLengthBits[i]);
      long sum = a + b + carry;
      sumBits[i] = (int) sum;
      carry = sum >>> 32; // 1 or 0
    }
    // carry == 0, no overflow possible anymore, just copy the bits from largerLengthBits, if available,  to sumBits
    if (carry == 0) return copyRemaining(largerLengthBits, sumBits, smallerLengthBits.length);
    // adding remaining bits (if available) from largerLengthBits
    for (int i = smallerLengthBits.length; i < largerLengthBits.length; i++) {
      long b = Integer.toUnsignedLong(largerLengthBits[i]);
      long sum = b + carry;
      sumBits[i] = (int) sum;
      carry = sum >>> 32; // 1 or 0
      if (carry == 0) return copyRemaining(largerLengthBits, sumBits, i + 1);
    }
    // here always carry == 1, the sum bits array has to be increased by 1
    sumBits = Arrays.copyOf(sumBits, sumBits.length + 1);
    sumBits[sumBits.length - 1] = 1;
    return sumBits;
  }

  private int[] copyRemaining(int[] src, int[] dst, int pos) {
    assert src.length == dst.length;
    System.arraycopy(src, pos, dst, pos, src.length - pos);
    return dst;
  }

  /**
   * Returns a BigInt whose value is {@code (this - subtrahend)}.
   *
   * @param subtrahend value to subtract from this BigInt.
   * @return {@code this - subtrahend}
   */
  public BigInt subtract(BigInt subtrahend) {
    if (isZero()) {
      if (subtrahend.isZero()) return ZERO;
      return new BigInt(subtrahend.bits, -sign);
    }
    if (subtrahend.isZero()) return this;
    int resultSign = compareBits(bits, subtrahend.bits);
    if (resultSign == 0) {
      if (sign == subtrahend.sign) return ZERO;
      return shiftLeft();
    }
    int[] largerNumberBits = bits, smallerNumberBits = subtrahend.bits;
    // ensure that always the greater number is subtracted by the smaller
    if (resultSign == -1) {
      largerNumberBits = subtrahend.bits;
      smallerNumberBits = bits;
    }
    int[] resultBits = new int[largerNumberBits.length];
    long borrow = 0;
    for (int i = 0; i < smallerNumberBits.length; i++) {
      long a = Integer.toUnsignedLong(largerNumberBits[i]) + TWO_POW_32;
      long b = Integer.toUnsignedLong(smallerNumberBits[i]);
      long difference = a - (b + borrow);
      resultBits[i] = (int) difference;
      borrow = 1 - (difference >> 32);
    }
    // now the ints from largerNumberBits and possible borrow
    for (int i = smallerNumberBits.length; i < largerNumberBits.length; i++) {
      long a = Integer.toUnsignedLong(largerNumberBits[i]) + TWO_POW_32;
      long difference = a - borrow;
      resultBits[i] = (int) difference;
      borrow = 1 - (difference >> 32);
    }
    resultBits = removeLeadingZeros(resultBits);
    return new BigInt(resultBits, resultSign);
  }

  /**
   * Returns a BigInt whose value is {@code (this * multiplier)}.
   *
   * <p>pencil and paper method, O(n²)
   *
   * @param multiplier value by which this BigInt is to be multiplied.
   * @return {@code this * multiplier}
   */
  public BigInt multiply(BigInt multiplier) {
    if (this.isZero() || multiplier.isZero()) return ZERO;
    if (this.equals(ONE)) return multiplier;
    if (multiplier.equals(ONE)) return this;

    int[] resultBits = new int[multiplier.bits.length + bits.length];
    for (int i = 0; i < multiplier.bits.length; i++) {
      long c = 0;
      final long r = Integer.toUnsignedLong(multiplier.bits[i]);
      for (int j = 0; j < bits.length; j++) {
        final long l = Integer.toUnsignedLong(bits[j]);
        final long p = l * r;
        final long sum = Integer.toUnsignedLong(resultBits[i + j]) + (p & LOW_32BIT_LONG_MASK) + c;
        resultBits[i + j] = (int) sum;
        c = sum >>> 32;
      }
      resultBits[i + bits.length] = (int) c;
    }
    resultBits = removeLeadingZeros(resultBits);
    return new BigInt(resultBits, this.sign * multiplier.sign);
  }

  private int[] removeLeadingZeros(int[] array) {
    int i = array.length - 1;
    while (array[i] == 0) i--;
    if (i != (array.length - 1)) array = Arrays.copyOf(array, i + 1);
    return array;
  }

  /**
   * Returns a BigInt whose value is {@code (this / divisor)}.
   *
   * @param divisor value by which this BigInt is to be divided.
   * @return {@code this / divisor}
   * @throws ArithmeticException if {@code divisor} is zero.
   */
  public BigInt divide(BigInt divisor) {
    if (divisor.isZero()) throw new ArithmeticException("Division by 0");
    if (isZero()) return this;
    if (divisor.equals(ONE)) return this;
    int compareResult = compareBits(this.bits, divisor.bits);
    if (compareResult < 0) return ZERO;
    if (compareResult == 0) return ONE;

    BigInt positiveQuotient = ZERO;
    BigInt positiveRemainder = sign == -1 ? this.negate() : this;
    BigInt positiveDivisor = divisor.sign == -1 ? divisor.negate() : divisor;

    int msbDivisor = presentMSB(positiveDivisor);
    while (compareBits(positiveRemainder.bits, positiveDivisor.bits) >= 0) {
      int msbRemainder = presentMSB(positiveRemainder);
      int leftShifts = msbRemainder - msbDivisor;
      BigInt subtrahend = positiveDivisor.shiftLeft(leftShifts);
      compareResult = compareBits(positiveRemainder.bits, subtrahend.bits);
      if (compareResult < 0) subtrahend = positiveDivisor.shiftLeft(--leftShifts);

      positiveRemainder = positiveRemainder.subtract(subtrahend);
      positiveQuotient = positiveQuotient.add(BigInt.ONE.shiftLeft(leftShifts));
    }
    return new BigInt(positiveQuotient.bits, this.sign * divisor.sign);
  }

  private int presentMSB(BigInt number) {
    int highestInt = number.bits[number.bits.length - 1];
    return ((number.bits.length - 1) * 32) + (31 - Integer.numberOfLeadingZeros(highestInt));
  }

  /**
   * Returns a BigInt[] with BigInt[0] = {@code (this / divisor)} and BigInt[1] = the remainder of
   * the division.
   *
   * @param divisor value by which this BigInt is to be divided.
   * @return {@code this / divisor} and the remainder of the division as a BigInt[]
   * @throws ArithmeticException if {@code divisor} is zero.
   */
  public BigInt[] divideAndRemainder(final BigInt divisor) {
    if (divisor.isZero()) throw new ArithmeticException("Division by 0");
    if (isZero()) return new BigInt[] {this, this};
    if (divisor.equals(ONE)) return new BigInt[] {this, ZERO};
    int compareResult = compareBits(this.bits, divisor.bits);
    if (compareResult < 0) return new BigInt[] {ZERO, this};
    if (compareResult == 0) return new BigInt[] {ONE, ZERO};

    BigInt positiveQuotient = ZERO;
    BigInt positiveRemainder = sign == -1 ? this.negate() : this;
    BigInt positiveDivisor = divisor.sign == -1 ? divisor.negate() : divisor;

    final int msbDivisor = presentMSB(positiveDivisor);
    while (compareBits(positiveRemainder.bits, positiveDivisor.bits) >= 0) {
      final int msbRemainder = presentMSB(positiveRemainder);
      int leftShifts = msbRemainder - msbDivisor;
      BigInt subtrahend = positiveDivisor.shiftLeft(leftShifts);
      compareResult = compareBits(positiveRemainder.bits, subtrahend.bits);
      if (compareResult < 0) subtrahend = positiveDivisor.shiftLeft(--leftShifts);

      positiveRemainder = positiveRemainder.subtract(subtrahend);
      positiveQuotient = positiveQuotient.add(BigInt.ONE.shiftLeft(leftShifts));
    }
    BigInt quotient = new BigInt(positiveQuotient.bits, this.sign * divisor.sign);
    BigInt remainder = this.sign == -1 ? positiveRemainder.negate() : positiveRemainder;
    return new BigInt[] {quotient, remainder};
  }

  /**
   * Returns a BigInt whose value is {@code (this mod divisor)}. This value is always positive or
   * ZERO.
   *
   * @param divisor value by which this BigInt is to be divided.
   * @return {@code this mod divisor}
   * @throws ArithmeticException if {@code divisor} is zero.
   */
  public BigInt mod(BigInt divisor) {
    if (divisor.isZero()) throw new ArithmeticException("Division by 0");
    if (isZero()) return this;
    if (divisor.equals(ONE)) return ZERO;
    int compareResult = compareBits(this.bits, divisor.bits);
    if (compareResult < 0) return this;
    if (compareResult == 0) return ZERO;

    BigInt positiveRemainder = sign == -1 ? this.negate() : this;
    BigInt positiveDivisor = divisor.sign == -1 ? divisor.negate() : divisor;
    int msbDivisor = presentMSB(positiveDivisor);
    while (compareBits(positiveRemainder.bits, positiveDivisor.bits) >= 0) {
      int msbRemainder = presentMSB(positiveRemainder);
      int leftShifts = msbRemainder - msbDivisor;
      BigInt subtrahend = positiveDivisor.shiftLeft(leftShifts);
      compareResult = compareBits(positiveRemainder.bits, subtrahend.bits);
      if (compareResult < 0) subtrahend = subtrahend.shiftRight();

      positiveRemainder = positiveRemainder.subtract(subtrahend);
    }
    return positiveRemainder;
  }

  /**
   * Returns a BigInt whose value is {@code (this << n)}. The shift distance, {@code distance}, may
   * be negative, in which case this method performs a right shift.
   *
   * @param distance shift distance, in bits.
   * @return {@code this << n}
   */
  public BigInt shiftLeft(int distance) {
    if (isZero() || (distance == 0)) return this;
    if (distance < 0) shiftRight(-distance);
    if (distance == 1) return shiftLeft();

    int intShifts = distance / 32, bitShifts = distance % 32;
    final int[] resultBits;
    if (bitShifts == 0) {
      resultBits = new int[bits.length + intShifts];
      System.arraycopy(bits, 0, resultBits, intShifts, bits.length);
    } else {
      int extraIntCount = carryBitsLeft(bits[bits.length - 1], bitShifts) > 0 ? 1 : 0;
      resultBits = new int[bits.length + intShifts + extraIntCount];
      int carry = 0;
      for (int i = 0; i < bits.length; i++) {
        resultBits[i + intShifts] = (bits[i] << bitShifts) | carry;
        carry = carryBitsLeft(bits[i], bitShifts);
      }
      resultBits[resultBits.length - 1] |= carry;
    }
    return new BigInt(resultBits, sign);
  }

  private int carryBitsLeft(int value, int leftShifts) {
    assert (leftShifts > 0) && (leftShifts < 32);
    return value >>> (32 - leftShifts);
  }

  /**
   * Returns a BigInt whose value is {@code (this << 1)}.
   *
   * @return {@code this << 1}
   */
  public BigInt shiftLeft() {
    if (isZero()) return this;

    int[] resultBits = Arrays.copyOf(bits, bits.length + (bits[bits.length - 1] >>> 31));
    int carry = 0;
    for (int i = 0; i < bits.length; i++) {
      resultBits[i] <<= 1;
      resultBits[i] |= carry;
      carry = bits[i] >>> 31;
    }
    resultBits[resultBits.length - 1] |= carry;
    return new BigInt(resultBits, sign);
  }

  /**
   * Returns a BigInt whose value is {@code (this >> n)}. The shift distance, {@code distance}, may
   * be negative, in which case this method performs a left shift.
   *
   * @param distance shift distance, in bits.
   * @return {@code this << n}
   */
  public BigInt shiftRight(int distance) {
    if (isZero() || (distance == 0)) return this;
    if (distance < 0) shiftLeft(-distance);
    if (distance == 1) return shiftRight();
    if (bits.length == 1) {
      int value = bits[0] >>> distance;
      if (value == 0) return ZERO;
      return new BigInt(new int[] {value}, sign);
    }
    int intShifts = distance / 32, bitShifts = distance % 32;
    final int[] resultBits;
    if (bitShifts == 0) {
      resultBits = new int[bits.length - intShifts];
      System.arraycopy(bits, intShifts, resultBits, 0, resultBits.length);
    } else {
      int highestInt = bits[bits.length - 1];
      int resultBitsLength = bits.length - intShifts - ((highestInt >> bitShifts) == 0 ? 1 : 0);
      resultBits = new int[resultBitsLength];
      int bitmask = (1 << bitShifts) - 1;
      int carry = highestInt & bitmask;
      for (int i = resultBits.length - 1; i >= 0; i--) {
        resultBits[i] = bits[i + intShifts] >>> bitShifts;
        resultBits[i] |= carry << (32 - bitShifts);
        carry = bits[i + intShifts] & bitmask;
      }
    }
    return new BigInt(resultBits, sign);
  }

  /**
   * Returns a BigInt whose value is {@code (this >> 1)}.
   *
   * @return {@code this >> 1}
   */
  public BigInt shiftRight() {
    if (isZero()) return this;
    if (bits.length == 1) {
      int value = bits[0] >>> 1;
      if (value == 0) return ZERO;
      return new BigInt(new int[] {value}, sign);
    }
    // if highest int == 1, the result bits will have one less int, also carry the 1
    int carry = bits[bits.length - 1] == 1 ? 1 : 0;
    final int[] resultBits = new int[bits.length - carry];
    for (int i = resultBits.length - 1; i >= 0; i--) {
      resultBits[i] = bits[i] >>> 1;
      resultBits[i] |= carry << 31;
      carry = bits[i] & 1;
    }
    return new BigInt(resultBits, sign);
  }

  @Override
  public int compareTo(BigInt other) {
    if (sign != other.sign) {
      if (sign == -1) return -1;
      if (sign == 1) return 1;
      return -other.sign;
    }
    return compareBits(bits, other.bits);
  }

  private static int compareBits(int[] bitsA, int[] bitsB) {
    if (bitsA.length < bitsB.length) return -1;
    if (bitsA.length > bitsB.length) return 1;
    for (int i = bitsA.length - 1; i >= 0; i--) {
      long a = Integer.toUnsignedLong(bitsA[i]);
      long b = Integer.toUnsignedLong(bitsB[i]);
      if (a < b) return -1;
      if (a > b) return 1;
    }
    return 0;
  }

  @Override
  public int hashCode() {
    int hashCode = 0;
    for (int i = 0; i < bits.length; i++) hashCode = (31 * hashCode) + bits[i];
    return hashCode * sign;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof BigInt) {
      BigInt other = ((BigInt) obj);
      if (sign != other.sign) return false;
      if (bits.length != other.bits.length) return false;
      return Arrays.equals(bits, other.bits);
    }
    return false;
  }

  @Override
  public String toString() {
    if (isZero()) return "0";
    StringBuilder sb = new StringBuilder();
    BigInt remaining = this;
    while (!remaining.isZero()) {
      BigInt[] result = remaining.divideAndRemainder(BigInt.TEN);
      sb.insert(0, result[1].bits[0]);
      remaining = result[0];
    }
    if (sign == -1) sb.insert(0, '-');
    return sb.toString();
  }

  private boolean isZero() {
    return sign == 0;
  }
}
