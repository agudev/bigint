package de.psvmargs.bigint;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(SlowTest.class)
public class BigIntSlowTest {

  @Test
  public void lucasLehmerTest() {
    for (int i = 3; i < 768; i++) {
      BigInt m = BigInt.ONE.shiftLeft(i).subtract(BigInt.ONE);
      BigInt s = BigInt.of(4);
      int c = i - 2;
      while (c > 0) {
        s = s.multiply(s).subtract(BigInt.TWO).mod(m);
        c--;
      }
      if (s.equals(BigInt.ZERO)) System.out.println(m);
    }
  }

  @Test
  public void lucasLehmerTestJDK() {
    BigInteger TWO = BigInteger.valueOf(2);
    for (int i = 3; i < 768; i++) {
      BigInteger m = BigInteger.ONE.shiftLeft(i).subtract(BigInteger.ONE);
      BigInteger s = BigInteger.valueOf(4);
      int c = i - 2;
      while (c > 0) {
        s = s.multiply(s).subtract(TWO).mod(m);
        c--;
      }
      if (s.equals(BigInteger.ZERO)) System.out.println(m);
    }
  }

  @Test
  public void lucasLehmerTestBoth() {
    BigInteger TWO = BigInteger.valueOf(2);

    for (int i = 3; i < 256; i++) {
      BigInteger m_jdk = BigInteger.ONE.shiftLeft(i).subtract(BigInteger.ONE);
      BigInteger s_jdk = BigInteger.valueOf(4);
      BigInt m = BigInt.ONE.shiftLeft(i).subtract(BigInt.ONE);
      BigInt s = BigInt.of(4);
      int c = i - 2;
      while (c > 0) {
        s_jdk = s_jdk.multiply(s_jdk).subtract(TWO).mod(m_jdk);
        s = s.multiply(s).subtract(BigInt.TWO).mod(m);
        assertThat(s.toString()).isEqualTo(s_jdk.toString());
        c--;
      }
      if (s_jdk.equals(BigInteger.ZERO)) System.out.println(m_jdk);
    }
  }

  @Test
  public void add() {
    for (int i = 0; i < 2_000; i++) {
      BigInteger a = new BigInteger(416, ThreadLocalRandom.current());
      BigInt b = BigInt.ofBinary(a.toString(2));
      a = a.add(a);
      b = b.add(b);
      assertThat(b.toString()).isEqualTo(a.toString());
    }
  }
}
