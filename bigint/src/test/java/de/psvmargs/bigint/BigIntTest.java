package de.psvmargs.bigint;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;

import org.junit.Test;

public class BigIntTest {

  @Test
  public void zero_plus_zero_should_be_zero() {
    BigInt a = BigInt.ZERO;
    BigInt b = BigInt.ZERO;
    assertThat(a.add(b)).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void one_plus_one_should_be_two() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.ONE;
    assertThat(a.add(b)).isEqualTo(BigInt.TWO);
  }

  @Test
  public void one_plus_two_should_be_three() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.TWO;
    assertThat(a.add(b)).isEqualTo(BigInt.of(3));
  }

  @Test
  public void one_plus_minus_two_should_be_minus_one() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.of(-2);
    assertThat(a.add(b)).isEqualTo(BigInt.of(-1));
  }

  @Test
  public void minus_one_plus_two_should_be_one() {
    BigInt a = BigInt.of(-1);
    BigInt b = BigInt.TWO;
    assertThat(a.add(b)).isEqualTo(BigInt.of(1));
  }

  @Test
  public void compare_zero_and_zero() {
    BigInt a = BigInt.ZERO;
    BigInt b = BigInt.ZERO;
    assertThat(a.compareTo(b)).isEqualTo(0);
    assertThat(b.compareTo(a)).isEqualTo(0);
  }

  @Test
  public void compare_one_and_two() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.TWO;
    assertThat(a.compareTo(b)).isEqualTo(-1);
    assertThat(b.compareTo(a)).isEqualTo(1);
  }

  @Test
  public void zero_minus_zero_should_be_zero() {
    BigInt a = BigInt.ZERO;
    BigInt b = BigInt.ZERO;
    assertThat(a.subtract(b)).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void one_minus_zero_should_be_one() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.ZERO;
    assertThat(a.subtract(b)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void two_minus_one_should_be_one() {
    BigInt a = BigInt.TWO;
    BigInt b = BigInt.ONE;
    assertThat(a.subtract(b)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void one_minus_two_should_be_minus_one() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.TWO;
    assertThat(a.subtract(b)).isEqualTo(BigInt.of(-1));
  }

  @Test
  public void two_pow_32_minus_one_should_me_max_unsigned_int_value() {
    BigInt a = BigInt.ONE.shiftLeft(32);
    BigInt b = BigInt.ONE;
    BigInt result = BigInt.of(new int[] {0xFFFFFFFF}, 1);
    assertThat(a.subtract(b)).isEqualTo(result);
  }

  @Test
  public void ofBinary_42() {
    BigInt a = BigInt.ofBinary("101010");
    assertThat(a).isEqualTo(BigInt.of(42));
  }

  @Test
  public void ofBinary_two_pow_32() {
    long a = 0b1_00000000_00000000_00000000_00000000L;
    BigInt b = BigInt.ofBinary(Long.toBinaryString(a));
    assertThat(b).isEqualTo(BigInt.ONE.shiftLeft(32));
  }

  @Test
  public void shiftLeft_one_by_zero_should_be_one() {
    assertThat(BigInt.ONE.shiftLeft(0)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void shiftLeft_one_by_one_should_be_2() {
    assertThat(BigInt.ONE.shiftLeft()).isEqualTo(BigInt.TWO);
    assertThat(BigInt.ONE.shiftLeft(1)).isEqualTo(BigInt.TWO);
  }

  @Test
  public void shiftLeft_highest_int_bit_should_be_two_pow_32() {
    BigInt a = BigInt.of(1 << 30).shiftLeft();
    assertThat(a.shiftLeft()).isEqualTo(BigInt.of(new int[] {0, 1}, 1));
  }

  @Test
  public void shiftLeft_one_by_8_should_be_256() {
    assertThat(BigInt.ONE.shiftLeft(8)).isEqualTo(BigInt.of(256));
  }

  @Test
  public void shiftLeft_one_by_32_should_be_two_pow_32() {
    assertThat(BigInt.ONE.shiftLeft(32)).isEqualTo(BigInt.of(new int[] {0, 1}, 1));
  }

  @Test
  public void shiftLeft_one_by_64_should_be_two_pow_64() {
    assertThat(BigInt.ONE.shiftLeft(64)).isEqualTo(BigInt.of(new int[] {0, 0, 1}, 1));
  }

  @Test
  public void shiftRight_two_pow_32_by_one_should_be_two_pwo_31() {
    BigInt a = BigInt.of(new int[] {0, 1}, 1);
    assertThat(a.shiftRight()).isEqualTo(BigInt.ONE.shiftLeft(31));
    assertThat(a.shiftRight(1)).isEqualTo(BigInt.ONE.shiftLeft(31));
  }

  @Test
  public void shiftRight_negative_two_by_one_should_be_negative_one() {
    assertThat(BigInt.of(-2).shiftRight()).isEqualTo(BigInt.ONE.negate());
    assertThat(BigInt.of(-2).shiftRight(1)).isEqualTo(BigInt.ONE.negate());
  }

  @Test
  public void shiftRight_one_by_zero_should_be_one() {
    assertThat(BigInt.ONE.shiftRight(0)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void shiftRight_one_by_one_should_be_zero() {
    assertThat(BigInt.ONE.shiftRight(1)).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void shiftRight_two_by_one_should_be_one() {
    assertThat(BigInt.TWO.shiftRight(1)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void shiftRight_two_pow_32_by_32_should_be_one() {
    assertThat(BigInt.of(new int[] {0, 1}, 1).shiftRight(32)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void shiftRight_two_pow_33_by_2_should_be_two_pow_31() {
    assertThat(BigInt.of(new int[] {0, 2}, 1).shiftRight(2)).isEqualTo(BigInt.ONE.shiftLeft(31));
  }

  @Test
  public void multiply_one_by_zero_should_be_zero() {
    assertThat(BigInt.ONE.multiply(BigInt.ZERO)).isEqualTo(BigInt.ZERO);
    assertThat(BigInt.ZERO.multiply(BigInt.ONE)).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void multiply_one_by_one_should_be_one() {
    assertThat(BigInt.ONE.multiply(BigInt.ONE)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void multiply_3_by_5_should_be_15() {
    BigInt a = BigInt.of(3);
    BigInt b = BigInt.of(5);
    assertThat(a.multiply(b)).isEqualTo(BigInt.of(15));
  }

  @Test
  public void multiply2_3_by_5_should_be_15() {
    BigInt a = BigInt.of(3);
    BigInt b = BigInt.of(5);
    assertThat(a.multiply(b)).isEqualTo(BigInt.of(15));
  }

  @Test
  public void multiply_one_shift_31_by_3_shift_30() {
    BigInt a = BigInt.ONE.shiftLeft(31);
    BigInt b = BigInt.of(3).shiftLeft(30);

    String result =
        BigInteger.valueOf(3).shiftLeft(30).multiply(BigInteger.ONE.shiftLeft(31)).toString();
    assertThat(a.multiply(b).toString()).isEqualTo(result);
  }

  @Test(expected = ArithmeticException.class)
  public void divideByZero_should_throw_exception() {
    BigInt a = BigInt.of(2);
    BigInt b = BigInt.ZERO;
    a.divide(b);
  }

  @Test
  public void divide_42_by_one_should_be_42() {
    BigInt a = BigInt.of(42);
    BigInt b = BigInt.ONE;
    assertThat(a.divide(b)).isEqualTo(a);
  }

  @Test
  public void divide_smaller_by_larger_should_be_zero() {
    BigInt a = BigInt.ONE;
    BigInt b = BigInt.TWO;
    assertThat(a.divide(b)).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void divide_ten_by_ten_should_be_one() {
    BigInt a = BigInt.TEN;
    BigInt b = BigInt.TEN;
    assertThat(a.divide(b)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void divide_negative_17_by_5_should_be_negative_3() {
    BigInt a = BigInt.of(-17);
    BigInt b = BigInt.of(5);
    assertThat(a.divide(b)).isEqualTo(BigInt.of(-3));
  }

  @Test
  public void basic_operations() {
    BigInt a = BigInt.of(18);
    BigInt b = BigInt.of(17);
    BigInt c = BigInt.TWO;
    BigInt d = BigInt.of(47);
    BigInt e = BigInt.of(150);
    // (((18 * 17) / 2) + 47) - 150 = 50
    assertThat(a.multiply(b).divide(c).add(d).subtract(e)).isEqualTo(BigInt.of(50));
  }

  @Test
  public void one_mod_10_should_be_one() {
    BigInt a = BigInt.of(1);
    BigInt b = BigInt.of(10);
    assertThat(a.mod(b)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void five_mod_two_should_be_one() {
    BigInt a = BigInt.of(5);
    BigInt b = BigInt.of(2);
    assertThat(a.mod(b)).isEqualTo(BigInt.ONE);
  }

  @Test
  public void twenty_five_mod_13_should_be_12() {
    BigInt a = BigInt.of(25);
    BigInt b = BigInt.of(13);
    assertThat(a.mod(b)).isEqualTo(BigInt.of(12));
  }

  @Test
  public void ofBinary_negative_zero_should_be_zero() {
    assertThat(BigInt.ofBinary("-0")).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void ofBinary_positive_zero_should_be_zero() {
    assertThat(BigInt.ofBinary("+0")).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void ofBinary_zero_should_be_zero() {
    assertThat(BigInt.ofBinary("0")).isEqualTo(BigInt.ZERO);
  }

  @Test
  public void divideAndRemainer_one_by_one_should_be_one_and_zero() {
    BigInt[] result = {BigInt.ONE, BigInt.ZERO};
    assertThat(BigInt.ONE.divideAndRemainder(BigInt.ONE)).isEqualTo(result);
  }

  @Test
  public void divideAndRemainer_one_by_two_should_be_zero_and_one() {
    BigInt[] result = {BigInt.ZERO, BigInt.ONE};
    assertThat(BigInt.ONE.divideAndRemainder(BigInt.TWO)).isEqualTo(result);
  }

  @Test
  public void divideAndRemainer_17_by_5() {
    BigInt[] result = {BigInt.of(3), BigInt.TWO};
    assertThat(BigInt.of(17).divideAndRemainder(BigInt.of(5))).isEqualTo(result);
  }

  @Test
  public void divideAndRemainer_17_by_minus_5() {
    BigInt[] expectedResult = {BigInt.of(-3), BigInt.TWO};
    assertThat(BigInt.of(17).divideAndRemainder(BigInt.of(-5))).isEqualTo(expectedResult);
  }

  @Test
  public void divideAndRemainer_minus_17_by_5() {
    BigInt[] expectedResult = {BigInt.of(-3), BigInt.TWO.negate()};
    assertThat(BigInt.of(-17).divideAndRemainder(BigInt.of(5))).isEqualTo(expectedResult);
  }

  @Test
  public void divideAndRemainer_minus_17_by_minus_5() {
    BigInt[] expectedResult = {BigInt.of(3), BigInt.TWO.negate()};
    assertThat(BigInt.of(-17).divideAndRemainder(BigInt.of(-5))).isEqualTo(expectedResult);
  }

  @Test
  public void mod_always_positive() {
    assertThat(BigInt.of(-17).mod(BigInt.of(5))).isEqualTo(BigInt.TWO);
    assertThat(BigInt.of(-17).mod(BigInt.of(-5))).isEqualTo(BigInt.TWO);
    assertThat(BigInt.of(17).mod(BigInt.of(5))).isEqualTo(BigInt.TWO);
    assertThat(BigInt.of(17).mod(BigInt.of(-5))).isEqualTo(BigInt.TWO);
  }

  @Test
  public void toString_tests() {
    assertThat(BigInt.of(0).toString()).isEqualTo("0");
    assertThat(BigInt.of(1).toString()).isEqualTo("1");
    assertThat(BigInt.of(-1).toString()).isEqualTo("-1");
    assertThat(BigInt.of(1234).toString()).isEqualTo("1234");
    assertThat(BigInt.of(-1234).toString()).isEqualTo("-1234");
    assertThat(BigInt.of(1234567890).toString()).isEqualTo("1234567890");
    assertThat(BigInt.of(-1234567890).toString()).isEqualTo("-1234567890");

    BigInt a = BigInt.of(111111);
    assertThat(a.multiply(a).toString()).isEqualTo("12345654321");
  }
}
